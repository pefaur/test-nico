import { NgModule } from '@angular/core';
import { TabindexDirective } from './tabindex/tabindex';
@NgModule({
	declarations: [TabindexDirective],
	imports: [],
	exports: [TabindexDirective]
})
export class DirectivesModule {}
