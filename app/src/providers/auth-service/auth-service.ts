import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class AuthService {

  public endPoint = "http://138.197.196.64:3002/";

  constructor(public http: HttpClient, private storage: Storage) {}

  /** login */
  login(user: { email: string, password: string }) {
    let login = this.http.post(this.endPoint+'auth/login', user);
    login.subscribe(
      data => {
        this.storage.set('token', data.token);
      }
    );
    return login;
  }

  /** logout */
  logout() {}
}
