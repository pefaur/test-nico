import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/observable/fromPromise';

@Injectable()
export class GameService {

  public endPoint = "http://138.197.196.64:3002/";
  constructor(public http: HttpClient, private storage: Storage) {}

  /** get all games */
  getAll(body?: {}) {
      return this.get('game/', body);
  }

  /** create game */
  create(body?: {}) {
    return this.post('game/', body);
  }

  /** get token */
  private getAuthToken() {
    return Observable.fromPromise(this.storage.get('token'));
  }

  /** GET */
  private get(url:string, body?:{}) {
    return this.getAuthToken().flatMap( token => {
      return  this.http.get(this.endPoint+url, {
        params: body,
        headers: new HttpHeaders().append('x-access-token', token)
      });
    })
  }
  /** POST */
  private post(url:string, body?:{}) {
    return this.getAuthToken().flatMap( token => {
      return  this.http.post(this.endPoint+url, body,{
        headers: new HttpHeaders().append('x-access-token', token)
      });
    })
  }

  /*
  private request(method:string, url:string, body?:any) {
    return this.getAuthToken().flatMap( token => {
      const header =  new HttpHeaders().append('x-access-token', token);
      const req = new HttpRequest(method, this.endPoint+url, body,{ headers: header });
      return this.http.request(req);
    });
  }*/
}
