import {Component} from '@angular/core';
import {IonicPage, NavController, AlertController, LoadingController, Loading} from 'ionic-angular';
import {HomePage} from "../home/home";
import {AuthService} from "../../providers/auth-service/auth-service";


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public user: { email: string, password: string } = {
    email: '',
    password: ''
  };
  public loading: Loading;
  public loginUrl = "http://138.197.196.64:3002/auth/login/";

  constructor(public navCtrl: NavController,
              private alertCtrl: AlertController,
              private loadingCtrl: LoadingController,
              private auth: AuthService) {}

  ionViewDidLoad() {}

  /**  Login */
  onLogin(event){
    /**  verificar Inputs*/
    if(!this.validateEmail(this.user.email)){
      this.alertCtrl.create({
        title: 'Email invalido',
        subTitle: 'verifique el formato del email',
        buttons: ['OK']
      }).present();
      return false;
    }else if((this.user.password).length == 0){
      this.alertCtrl.create({
        title: 'Contraseña en Blanco',
        subTitle: 'La contraseña no puede estar en blanco',
        buttons: ['OK']
      }).present();
      return false;
    }

    /**  Create Loader */
    this.loading = this.loadingCtrl.create({
      content: 'Iniciando Sesión...'
    });
    this.loading.present();

    /**  Login Auth */
    this.auth.login(this.user)
      .subscribe(data => {
          this.successLogin(data);
      },
      err => {
        this.errorLogin(err);
      });
  }

  /**  Success Login */
  successLogin(data) {
    this.navCtrl.setRoot(HomePage, {user: data});
    this.loading.dismiss();
  }

  /**  Error Login */
  errorLogin(error) {
    this.alertCtrl.create({
      title: 'Error al iniciar sesión',
      subTitle: 'Email o Contraseña inválida',
      buttons: ['OK']
    }).present();
    this.loading.dismiss();
  }

  // Validar Email
  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

}
