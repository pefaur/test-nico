import { Component } from '@angular/core';
import { IonicPage, NavParams, AlertController, LoadingController} from 'ionic-angular';
import { GameService } from "../../providers/game-service/game-service";


@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  public listGame = [];

  constructor(public navParams: NavParams,
              private alertCtrl: AlertController,
              private loadingCtrl: LoadingController,
              private game: GameService) {
  }

  ionViewDidLoad() {
    this.getGames(false);
  }

  getGames(refresher){
    /** Http loading subscribe */
    let loading = this.loadingCtrl.create({
      content: 'loading Games...'
    });
    loading.present();
    this.game.getAll()
      .subscribe(data => {
          /**  Success */
          this.listGame = data.docs;
          loading.dismiss();
          if(refresher) refresher.complete();
        },
        err => {
          loading.dismiss();
          if(refresher) refresher.complete();
          this.alertCtrl.create({
            title: 'Error',
            subTitle: 'Error al retornar lista de juegos',
            buttons: ['OK']
          }).present();
        });
  }
}
